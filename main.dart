import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/login.dart';

void main(theme) {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'My App',
        home: const Login(),
        theme: ThemeData(
            primarySwatch: Colors.cyan,
            hintColor: Colors.cyan,
            scaffoldBackgroundColor: Colors.black87));
  }
}
