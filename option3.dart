import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/dashboard.dart';

class Option3 extends StatelessWidget {
  const Option3({Key? key}) : super(key: key);

  static const String _title = "Option 3";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(_title),
          centerTitle: true,
        ),
        body: Column(
          children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(
                fit: StackFit.expand,
                clipBehavior: Clip.none,
                children: const [
                  CircleAvatar(
                    backgroundImage:
                        AssetImage("assets/Liam_circle_colour.png"),
                  )
                ],
              ),
            ),
            Container(
                alignment: Alignment.bottomRight,
                padding: const EdgeInsets.all(10),
                child: ElevatedButton(
                  child: const Text("Home"),
                  onPressed: () => {
                    Navigator.pop(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Dashboard()))
                  },
                ))
          ],
        ));
  }
}
