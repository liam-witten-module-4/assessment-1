import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/splash.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  static const String _title = 'Login';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(_title),
        centerTitle: true,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 115,
            width: 115,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
              children: const [
                CircleAvatar(
                  backgroundImage: AssetImage("assets/Liam_circle_colour.png"),
                )
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
              style: TextStyle(color: Colors.cyan),
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                  labelText: 'Username'),
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
              style: TextStyle(color: Colors.cyan),
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                  labelText: 'Email Address'),
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
              obscureText: true,
              style: TextStyle(color: Colors.cyan),
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                  labelText: 'Password'),
            ),
          ),
          Container(
              height: 60,
              padding: const EdgeInsets.all(10),
              child: ElevatedButton(
                child: const Text('Login'),
                onPressed: () => {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SplashScreen()))
                },
              )),
        ],
      ),
    );
  }
}
